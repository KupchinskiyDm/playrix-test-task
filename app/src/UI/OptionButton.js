class OptionButton {
    constructor (content, index) {
        this._enabled = false;
        this._selected = false;
        this._index = index;
        this._callbackTarget = null;
        this._onSelect = null;
        this._onComplete = null;
        this._action = content.getActionByTag(content.getTag());
        this._content = getChildByPath(content,'content');
        this._hitzone = getChildByPath(content,'hitzone');
        this._hitzone.addTouchEventListener(this._onClick, this);
        this._tile = getChildByPath(this._content,'tile');
        this._tile.getActionByTag(this._tile.getTag()).gotoFrameAndPause(index);
        this._okbutton = new Button(getChildByPath(this._content,'ok_button'));
        this._okbutton.callbackTarget = this;
        this._okbutton.onClickCallback = this._onOptionCompleted;
    }

    set onSelect (value){
        this._onSelect = value;
    }

    set onComplete (value){
        this._onComplete = value;
    }

    set callbackTarget (value){
        this._callbackTarget = value;
    }

    set enabled (value){
        this._enabled = value;
    }

    get enabled (){
        return this._enabled;
    }

    get index () {
        return this._index;
    }

    set selected (value){
        this._selected = value;
        this._action.play(value ? 'select' : 'deselect', false);
        this._okbutton.enabled = value;
    }

    get selected (){
        return this._selected;
    }

    _onOptionSelected (index) {
        if (this._callbackTarget && this._onSelect){
            this._onSelect.call(this._callbackTarget,index)
        }
    }

    _onOptionCompleted () {
        if (this._callbackTarget && this._onComplete){
            this._onComplete.call(this._callbackTarget,this._index)
        }
    }

    _onClick (sender, type) {
        if (type === ccui.Widget.TOUCH_ENDED && this._enabled && !this._selected) {
            this.selected = true;
            this._onOptionSelected.call(this, this._index);
        }
    }
}