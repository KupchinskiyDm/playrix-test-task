class RadioPanel {
    constructor (content) {
        this._onSelect = null;
        this._onComplete = null;
        this._callbackTarget = null;
        this._content = getChildByPath(content,'content');
        this._action = content.getActionByTag(content.getTag());
        this._options = this._content.getChildren().map((option, index) => {
            const button = new OptionButton(option, index);
            button.onSelect = this._onOptionSelected;
            button.onComplete = this._onOptionCompleted;
            button.callbackTarget = this;
            return button;
        })
    }
    
    show () {
        if (this._action){
            this._action.setFrameEventCallFunc(frame => {
                if (frame && frame._event === "end") {
                    this._options.forEach(option => option.enabled = true);
                }
            });
            this._action.play('show', false);
        }
    }

    set callbackTarget (value){
        this._callbackTarget = value;
    }

    set onSelect (value){
        this._onSelect = value;
    }

    set onComplete (value){
        this._onComplete = value;
    }

    _onOptionSelected (index) {
        if (this._callbackTarget, this._onSelect){
            this._onSelect.call(this._callbackTarget, index)
        }
        this._options.filter(option => {
            return option.index !== index && option.selected
        }).map(option => {
            option.selected = false;
        })
    }

    _onOptionCompleted (index) {
        this._action.stop();
        this._options.filter(option => {
            option.enabled = false;
            return option.selected
        }).map(option => {
            option.selected = false;
        })
        this._action.play('hide', false);
        if (this._callbackTarget, this._onComplete){
            this._onComplete.call(this._callbackTarget, index)
        }
    }


}