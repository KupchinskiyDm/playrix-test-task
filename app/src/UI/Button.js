class Button {
    constructor (content, index) {
        this._enabled = false;
        this._callbackTarget = null;
        this._onClickCallback = null;
        this._palyAction = true;
        this._action = content.getActionByTag(content.getTag());
        this._hitzone = getChildByPath(content,'hitzone');
        this._hitzone.addTouchEventListener(this._onClick, this);
    }

    set callbackTarget (value){
        this._callbackTarget = value;
    }

    set playStates (value){
        this._palyAction = value;
    }

    set onClickCallback (value){
        this._onClickCallback = value;
    }

    set enabled (value){
        this._enabled = value;
    }

    get enabled (){
        return this._enabled;
    }

    _onClick (sender, type) {
        if (this._enabled) {
            switch (type){
                case ccui.Widget.TOUCH_BEGAN : 
                    if (this._palyAction) this._action.gotoFrameAndPause(1);
                    break;
                case ccui.Widget.TOUCH_CANCELED :
                    if (this._palyAction) this._action.gotoFrameAndPause(0);
                    break;
                case ccui.Widget.TOUCH_ENDED :
                    if (this._palyAction) this._action.gotoFrameAndPause(0);
                    if (this._onClickCallback && this._callbackTarget){
                        this._onClickCallback.call(this._callbackTarget);
                    }
                    break;
            }
        }
    }
}