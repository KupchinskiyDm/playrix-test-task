const getChildByPath = function (root, path) {
    let currentChild = root;
    path.split('/').filter(value => {
        return Boolean(value);
    }).map(childName =>{
        if (currentChild && childName) {
            currentChild = currentChild.getChildByName(childName);
        } else {
            currentChild = null;
        }
    });
    return currentChild;
};