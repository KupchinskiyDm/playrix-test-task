const PROJECT_RES_REF = "res/assets/";

class Resources {
    constructor() {
        this.list = [
            'res/config.json',
            `${PROJECT_RES_REF}MainScene.json`
        ];
    }

    loadResources (callback){
        cc.loader.load(this.list, err => {
            if (!err) callback();
        });
    }

    getResource (res) {
        const path = this.list.find(item => {return item === `${PROJECT_RES_REF}${res}`});
        return path ? ccs.load(path) : null;
    }

    getConfig () {
        return cc.loader.getRes('res/config.json');
    }
}
