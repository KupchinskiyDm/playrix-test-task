class InteriorElement {
    constructor (content){
        this._action = content.getActionByTag(content.getTag());
        this._content = getChildByPath(content,'content');
    }

    show (index){
        const state = this._content.getActionByTag(this._content.getTag());
        state.gotoFrameAndPause(index);
        this._action.stop();
        this._action.play('appear', false);
    }
}