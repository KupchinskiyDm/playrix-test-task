
const MainScreen = cc.Scene.extend({
    resources: null,
    layer: null,
    scene: null,
    action: null,
    inputListener : null,

    onEnter: function () {
        this._super();

        const self = this;

        this.layer = new cc.Layer();
        this.addChild(this.layer);
        
        this.resources = new Resources();
        this.resources.loadResources(() => self.initScreen());
    },

    initScreen: function () {
        const sceneRes = this.resources.getResource('MainScene.json');
        
        this.scene = sceneRes.node,
        this.action = sceneRes.action
        this.layer.addChild(this.scene, 0);
        this.scene.runAction(this.action);

        this.continueButton = getChildByPath(this.scene, 'screen/bouncingButton')
        this.easeContinueButton();

        this.stairsOptionPanel = new RadioPanel(getChildByPath(this.scene,'screen/options'));
        this.stairsOptionPanel.callbackTarget = this;
        this.stairsOptionPanel.onSelect = this.onStairSelected;
        this.stairsOptionPanel.onComplete = this.onStairsBuild;

        const buildBtnContent = getChildByPath(this.scene, 'screen/buildButton');
        this.buildButton = new Button(buildBtnContent)
        this.buildButton.callbackTarget = this;
        this.buildButton.playStates = false;
        this.buildButton.onClickCallback = this.onBuildStart;
        this.buildButtonAction = buildBtnContent.getActionByTag(buildBtnContent.getTag());
        this.buildButtonAction.setFrameEventCallFunc(frame => {
            if (frame && frame._event === "enable") {
                this.buildButton.enabled = true;
            }
        });
        this.action.setFrameEventCallFunc(frame => {
            if (frame && frame._event === "show_build_button") {
                this.buildButtonAction.play('intro', false)
            }
        });
        this.stairsElement = new InteriorElement(getChildByPath(this.scene, 'screen/stairs'))

        this.action.play('intro', false);
    },

    easeContinueButton : function (){
        // Just example of using ease effect by TweenLite lib
        // In regular situation I would make such animation by Cocos Studio instrumentation
        const self = this;
        TweenLite.to(this.continueButton, 1, {scale: 1.08, ease: Linear.easeIn,
            onComplete: function () {
                TweenLite.to(self.continueButton, 1, {scale: 1, ease: Linear.easeOut,
                    onComplete: function () {
                        self.easeContinueButton();
                    }
                })
            }
        })
    }, 

    onBuildStart : function (){
        this.stairsOptionPanel.show();
        this.buildButtonAction.play('hide', false);
        this.buildButton.enabled = false;
    },

    onStairSelected : function (index) {
        this.stairsElement.show(index);
    },

    onStairsBuild : function (index){
        this.action.play('final', false);
    }
});
