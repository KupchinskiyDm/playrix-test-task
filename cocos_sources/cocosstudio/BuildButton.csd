<GameFile>
  <PropertyGroup Name="BuildButton" Type="Node" ID="c4584098-9621-4635-b57c-195808a07f80" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="115" Speed="1.0000" ActivedAnimationName="intro">
        <Timeline ActionTag="-453035205" Property="Position">
          <PointFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="25" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-453035205" Property="Scale">
          <ScaleFrame FrameIndex="25" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-453035205" Property="RotationSkew">
          <ScaleFrame FrameIndex="25" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-453035205" Property="FrameEvent">
          <EventFrame FrameIndex="0" Tween="False" Value="" />
          <EventFrame FrameIndex="25" Tween="False" Value="enable" />
        </Timeline>
        <Timeline ActionTag="2086402170" Property="Position">
          <PointFrame FrameIndex="0" X="0.7642" Y="18.3313">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="2086402170" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.4882" Y="0.4882">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="2086402170" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2115200384" Property="Position">
          <PointFrame FrameIndex="100" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-2115200384" Property="Scale">
          <ScaleFrame FrameIndex="100" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2115200384" Property="RotationSkew">
          <ScaleFrame FrameIndex="100" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1331932025" Property="Position">
          <PointFrame FrameIndex="15" X="4.5832" Y="11.4572">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="25" X="4.5832" Y="11.4572">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="35" X="4.5832" Y="11.4572">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="100" X="4.5832" Y="11.4572">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1331932025" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.0001" Y="0.0001">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="15" X="0.0001" Y="0.0001">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="25" X="1.2000" Y="1.2000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="35" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="100" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1331932025" Property="RotationSkew">
          <ScaleFrame FrameIndex="15" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="25" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="35" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="100" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="655986046" Property="Position">
          <PointFrame FrameIndex="0" X="0.0000" Y="142.9966">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="15" X="0.0000" Y="-10.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="20" X="0.0000" Y="10.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="25" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="100" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="101" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="115" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="655986046" Property="Scale">
          <ScaleFrame FrameIndex="100" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="101" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="103" X="0.9310" Y="0.9310">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="115" X="0.0001" Y="0.0001">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="655986046" Property="RotationSkew">
          <ScaleFrame FrameIndex="100" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="101" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="115" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="655986046" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="15" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="100" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="101" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="115" Value="0">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
        <Timeline ActionTag="-1860948583" Property="ActionValue">
          <InnerActionFrame FrameIndex="0" Tween="False" InnerActionType="LoopAction" CurrentAniamtionName="bouncing" SingleFrameIndex="0" />
          <InnerActionFrame FrameIndex="101" Tween="False" InnerActionType="LoopAction" CurrentAniamtionName="bouncing" SingleFrameIndex="0" />
          <InnerActionFrame FrameIndex="102" Tween="False" InnerActionType="LoopAction" CurrentAniamtionName="bouncing" SingleFrameIndex="0" />
        </Timeline>
        <Timeline ActionTag="-1860948583" Property="Position">
          <PointFrame FrameIndex="0" X="-146.0210" Y="-61.1978">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="101" X="-146.0210" Y="-61.1978">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="102" X="-146.0210" Y="-61.1978">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-1860948583" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.6000" Y="0.6000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="101" X="0.6000" Y="0.6000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="102" X="0.6000" Y="0.6000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1860948583" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="35" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="47" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="101" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="102" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1860948583" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="35" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="47" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="101" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="102" Value="0">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
        <Timeline ActionTag="-1860948583" Property="VisibleForFrame">
          <BoolFrame FrameIndex="0" Tween="False" Value="True" />
          <BoolFrame FrameIndex="101" Tween="False" Value="False" />
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="intro" StartIndex="0" EndIndex="100">
          <RenderColor A="150" R="255" G="255" B="240" />
        </AnimationInfo>
        <AnimationInfo Name="hide" StartIndex="101" EndIndex="130">
          <RenderColor A="150" R="255" G="250" B="205" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Node" Tag="215" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="event" ActionTag="-453035205" Tag="220" IconVisible="True" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="hitzone" ActionTag="2086402170" Tag="216" IconVisible="False" LeftMargin="-99.2358" RightMargin="-100.7642" TopMargin="-118.3313" BottomMargin="-81.6687" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.7642" Y="18.3313" />
            <Scale ScaleX="0.4882" ScaleY="0.4882" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="content" ActionTag="655986046" Tag="218" IconVisible="True" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="hammer_bubble_1" CanEdit="False" ActionTag="-2115200384" Tag="217" IconVisible="False" LeftMargin="-53.0000" RightMargin="-53.0000" TopMargin="-64.5000" BottomMargin="-64.5000" ctype="SpriteObjectData">
                <Size X="106.0000" Y="129.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="textures/hammer_bubble.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="hammer_instrument_2" ActionTag="1331932025" Tag="219" IconVisible="False" LeftMargin="-28.9168" RightMargin="-38.0832" TopMargin="-45.9572" BottomMargin="-23.0428" ctype="SpriteObjectData">
                <Size X="67.0000" Y="69.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="4.5832" Y="11.4572" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="textures/hammer_instrument.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="pointer" ActionTag="-1860948583" Alpha="191" Tag="331" IconVisible="True" LeftMargin="-146.0210" RightMargin="146.0210" TopMargin="61.1978" BottomMargin="-61.1978" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-146.0210" Y="-61.1978" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="handPointer.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>