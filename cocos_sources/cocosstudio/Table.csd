<GameFile>
  <PropertyGroup Name="Table" Type="Node" ID="bb654b55-e648-4efd-a70f-342236fdc4cd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="30" Speed="1.0000">
        <Timeline ActionTag="-2124573912" Property="Position">
          <PointFrame FrameIndex="0" X="0.0000" Y="40.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="7" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-2124573912" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.6000" Y="1.3000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="7" X="0.6000" Y="1.3000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="14" X="1.1500" Y="0.8000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="25" X="1.0000" Y="1.1000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2124573912" Property="RotationSkew">
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2124573912" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="7" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="30" Value="255">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
        <Timeline ActionTag="-2124573912" Property="AnchorPoint">
          <ScaleFrame FrameIndex="7" X="0.4800" Y="0.0600">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.4800" Y="0.0600">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="intro" StartIndex="0" EndIndex="45">
          <RenderColor A="255" R="128" G="0" B="128" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Node" Tag="151" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Decor_4_1" ActionTag="-2124573912" Alpha="0" Tag="152" IconVisible="False" LeftMargin="-120.0000" RightMargin="-130.0000" TopMargin="-219.5400" BottomMargin="28.5400" ctype="SpriteObjectData">
            <Size X="250.0000" Y="191.0000" />
            <AnchorPoint ScaleX="0.4800" ScaleY="0.0600" />
            <Position Y="40.0000" />
            <Scale ScaleX="0.6000" ScaleY="1.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="textures/Decor_4.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>