<GameFile>
  <PropertyGroup Name="Plant_0" Type="Node" ID="39fa1a4b-ff3a-4283-91d4-adf210cf70c2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="45" Speed="1.0000" ActivedAnimationName="intro">
        <Timeline ActionTag="1844780292" Property="Position">
          <PointFrame FrameIndex="19" X="-56.0000" Y="300.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="27" X="-56.0000" Y="156.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="-56.5389" Y="156.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1844780292" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.2420" Y="1.2971">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="19" X="0.2420" Y="1.3000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="27" X="0.2400" Y="1.3000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="1.3000" Y="0.4796">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="41" X="1.0000" Y="1.1000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="45" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1844780292" Property="RotationSkew">
          <ScaleFrame FrameIndex="19" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1844780292" Property="Alpha">
          <IntFrame FrameIndex="19" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="27" Value="255">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="intro" StartIndex="0" EndIndex="45">
          <RenderColor A="255" R="255" G="255" B="255" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Node" Tag="24" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="decor" ActionTag="-1058223917" Tag="25" IconVisible="True" LeftMargin="54.6524" RightMargin="-54.6524" TopMargin="184.8533" BottomMargin="-184.8533" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="plant_0" ActionTag="1844780292" Tag="26" IconVisible="False" LeftMargin="-125.2234" RightMargin="-14.7766" TopMargin="-287.5686" BottomMargin="136.5686" ctype="SpriteObjectData">
                <Size X="140.0000" Y="151.0000" />
                <AnchorPoint ScaleX="0.4906" ScaleY="0.1287" />
                <Position X="-56.5389" Y="156.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.1000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="textures/Decor_0.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="54.6524" Y="-184.8533" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>