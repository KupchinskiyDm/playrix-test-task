<GameFile>
  <PropertyGroup Name="MainScene" Type="Scene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="235" Speed="1.0000" ActivedAnimationName="final">
        <Timeline ActionTag="854422190" Property="VisibleForFrame">
          <BoolFrame FrameIndex="0" Tween="False" Value="True" />
        </Timeline>
        <Timeline ActionTag="11947084" Property="ActionValue">
          <InnerActionFrame FrameIndex="0" Tween="False" InnerActionType="SingleFrame" CurrentAniamtionName="intro" SingleFrameIndex="0" />
          <InnerActionFrame FrameIndex="50" Tween="False" InnerActionType="NoLoopAction" CurrentAniamtionName="intro" SingleFrameIndex="0" />
          <InnerActionFrame FrameIndex="123" Tween="False" InnerActionType="SingleFrame" CurrentAniamtionName="intro" SingleFrameIndex="45" />
        </Timeline>
        <Timeline ActionTag="11947084" Property="Scale">
          <ScaleFrame FrameIndex="123" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="11947084" Property="RotationSkew">
          <ScaleFrame FrameIndex="123" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-712315937" Property="ActionValue">
          <InnerActionFrame FrameIndex="0" Tween="False" InnerActionType="SingleFrame" CurrentAniamtionName="intro" SingleFrameIndex="0" />
          <InnerActionFrame FrameIndex="63" Tween="False" InnerActionType="NoLoopAction" CurrentAniamtionName="intro" SingleFrameIndex="0" />
          <InnerActionFrame FrameIndex="123" Tween="False" InnerActionType="SingleFrame" CurrentAniamtionName="intro" SingleFrameIndex="45" />
        </Timeline>
        <Timeline ActionTag="-712315937" Property="Scale">
          <ScaleFrame FrameIndex="123" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-712315937" Property="RotationSkew">
          <ScaleFrame FrameIndex="123" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1457585949" Property="ActionValue">
          <InnerActionFrame FrameIndex="0" Tween="False" InnerActionType="SingleFrame" CurrentAniamtionName="-- ALL --" SingleFrameIndex="0" />
          <InnerActionFrame FrameIndex="56" Tween="False" InnerActionType="NoLoopAction" CurrentAniamtionName="intro" SingleFrameIndex="0" />
        </Timeline>
        <Timeline ActionTag="-171420689" Property="ActionValue">
          <InnerActionFrame FrameIndex="0" Tween="False" InnerActionType="SingleFrame" CurrentAniamtionName="-- ALL --" SingleFrameIndex="0" />
          <InnerActionFrame FrameIndex="1" Tween="False" InnerActionType="SingleFrame" CurrentAniamtionName="-- ALL --" SingleFrameIndex="0" />
        </Timeline>
        <Timeline ActionTag="-171420689" Property="Position">
          <PointFrame FrameIndex="0" X="419.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="1" X="419.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-171420689" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="1" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-171420689" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="1" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="2053972932" Property="Position">
          <PointFrame FrameIndex="215" X="-715.0582" Y="-338.0288">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="230" X="-715.0582" Y="-338.0288">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="2053972932" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.2538" Y="1.0544">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="20" X="1.2538" Y="1.0544">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="215" X="1.2538" Y="1.0544">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="230" X="1.2538" Y="1.0544">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="2053972932" Property="RotationSkew">
          <ScaleFrame FrameIndex="20" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="215" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="230" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="2053972932" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="20" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="215" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="230" Value="135">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
        <Timeline ActionTag="832981202" Property="Scale">
          <ScaleFrame FrameIndex="215" X="0.0001" Y="0.0001">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="230" X="1.0500" Y="1.0500">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="235" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="832981202" Property="RotationSkew">
          <ScaleFrame FrameIndex="215" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="230" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="832981202" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="215" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="230" Value="255">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
        <Timeline ActionTag="832981202" Property="VisibleForFrame">
          <BoolFrame FrameIndex="0" Tween="False" Value="False" />
          <BoolFrame FrameIndex="215" Tween="False" Value="True" />
          <BoolFrame FrameIndex="230" Tween="False" Value="True" />
        </Timeline>
        <Timeline ActionTag="1200085937" Property="ActionValue">
          <InnerActionFrame FrameIndex="0" Tween="False" InnerActionType="SingleFrame" CurrentAniamtionName="bouncing" SingleFrameIndex="0" />
        </Timeline>
        <Timeline ActionTag="1200085937" Property="Position">
          <PointFrame FrameIndex="0" X="-10.8556" Y="-241.1318">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1200085937" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1200085937" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="721920633" Property="ActionValue">
          <InnerActionFrame FrameIndex="0" Tween="False" InnerActionType="SingleFrame" CurrentAniamtionName="-- ALL --" SingleFrameIndex="0" />
          <InnerActionFrame FrameIndex="20" Tween="False" InnerActionType="NoLoopAction" CurrentAniamtionName="intro" SingleFrameIndex="0" />
        </Timeline>
        <Timeline ActionTag="721920633" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="20" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="721920633" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="20" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-860491129" Property="Position">
          <PointFrame FrameIndex="0" X="568.0000" Y="320.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="110" X="568.0000" Y="320.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-860491129" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="110" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-860491129" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="110" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-860491129" Property="Alpha">
          <IntFrame FrameIndex="0" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="110" Value="255">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
        <Timeline ActionTag="-860491129" Property="FrameEvent">
          <EventFrame FrameIndex="110" Tween="False" Value="show_build_button" />
        </Timeline>
        <Timeline ActionTag="-1243310194" Property="Position">
          <PointFrame FrameIndex="0" X="-141.6647" Y="-19.8846">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="20" X="-141.6647" Y="-19.8846">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-1243310194" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.2538" Y="1.0544">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="20" X="1.2538" Y="1.0544">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1243310194" Property="RotationSkew">
          <ScaleFrame FrameIndex="20" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1243310194" Property="Alpha">
          <IntFrame FrameIndex="0" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="20" Value="0">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="intro" StartIndex="0" EndIndex="200">
          <RenderColor A="255" R="173" G="255" B="47" />
        </AnimationInfo>
        <AnimationInfo Name="final" StartIndex="201" EndIndex="255">
          <RenderColor A="255" R="255" G="127" B="80" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="1136.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="refs" CanEdit="False" ActionTag="-1522263356" VisibleForFrame="False" Tag="3" IconVisible="True" LeftMargin="568.0000" RightMargin="568.0000" TopMargin="320.0000" BottomMargin="320.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="scene" CanEdit="False" ActionTag="854422190" Tag="4" IconVisible="False" LeftMargin="-695.0000" RightMargin="-695.0000" TopMargin="-320.0000" BottomMargin="-320.0000" ctype="SpriteObjectData">
                <Size X="1390.0000" Y="640.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="textures/withdecor.png" Plist="" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="568.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="screen" ActionTag="-860491129" Tag="8" IconVisible="True" LeftMargin="568.0000" RightMargin="568.0000" TopMargin="320.0000" BottomMargin="320.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="background" CanEdit="False" ActionTag="-848256557" Tag="8" IconVisible="False" LeftMargin="-695.0000" RightMargin="-695.0000" TopMargin="-320.0000" BottomMargin="-320.0000" ctype="SpriteObjectData">
                <Size X="1390.0000" Y="640.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="textures/SceneBackground.png" Plist="" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="decor" CanEdit="False" ActionTag="1421197373" Tag="23" IconVisible="True" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="plant_0" CanEdit="False" ActionTag="11947084" Tag="27" IconVisible="True" LeftMargin="-103.5026" RightMargin="103.5026" TopMargin="-223.4935" BottomMargin="223.4935" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="-103.5026" Y="223.4935" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="Plant_0.csd" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="plant_1" CanEdit="False" ActionTag="-712315937" Tag="65" IconVisible="True" LeftMargin="493.1050" RightMargin="-493.1050" TopMargin="-52.8649" BottomMargin="52.8649" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="493.1050" Y="52.8649" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="Plant_0.csd" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="globe" CanEdit="False" ActionTag="378682866" Tag="124" IconVisible="False" LeftMargin="-608.0474" RightMargin="461.0474" TopMargin="-211.0140" BottomMargin="23.0140" ctype="SpriteObjectData">
                    <Size X="147.0000" Y="188.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-534.5474" Y="117.0140" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="textures/Decor_3.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="table" CanEdit="False" ActionTag="-1457585949" Tag="167" IconVisible="True" LeftMargin="-372.4019" RightMargin="372.4019" TopMargin="55.4368" BottomMargin="-55.4368" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="-372.4019" Y="-55.4368" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="Table.csd" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sofa" CanEdit="False" ActionTag="-1510835032" Tag="123" IconVisible="False" LeftMargin="-568.0381" RightMargin="193.0381" TopMargin="4.6581" BottomMargin="-311.6581" ctype="SpriteObjectData">
                    <Size X="375.0000" Y="307.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-380.5381" Y="-158.1581" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="textures/Decor_1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="book" CanEdit="False" ActionTag="2101643321" Tag="127" IconVisible="False" LeftMargin="139.4469" RightMargin="-279.4469" TopMargin="-348.0950" BottomMargin="159.0950" ctype="SpriteObjectData">
                    <Size X="140.0000" Y="189.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="209.4469" Y="253.5950" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="textures/Decor_2.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="hero" CanEdit="False" ActionTag="726504580" Tag="125" IconVisible="True" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="Austin" CanEdit="False" ActionTag="-1662145194" Tag="126" IconVisible="False" LeftMargin="0.3851" RightMargin="-87.3851" TopMargin="-205.9091" BottomMargin="-100.0909" ctype="SpriteObjectData">
                    <Size X="87.0000" Y="306.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="43.8851" Y="52.9091" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="textures/Austin.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="stairs" CanEdit="False" ActionTag="1700498378" Tag="139" IconVisible="True" LeftMargin="526.3384" RightMargin="-526.3384" TopMargin="68.1258" BottomMargin="-68.1258" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="526.3384" Y="-68.1258" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Stairs.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="options" CanEdit="False" ActionTag="1647936518" Tag="756" IconVisible="True" LeftMargin="314.9823" RightMargin="-314.9823" TopMargin="-154.6553" BottomMargin="154.6553" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="314.9823" Y="154.6553" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="OptionsPanel.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="plant_decor_front" CanEdit="False" ActionTag="1252263717" Tag="142" IconVisible="False" LeftMargin="425.6241" RightMargin="-591.6241" TopMargin="116.0403" BottomMargin="-360.0403" ctype="SpriteObjectData">
                <Size X="166.0000" Y="244.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="508.6241" Y="-238.0403" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="textures/Decor_5.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="buildButton" CanEdit="False" ActionTag="-171420689" Tag="251" IconVisible="True" LeftMargin="419.0000" RightMargin="-419.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="419.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="BuildButton.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="shadow" ActionTag="2053972932" Alpha="0" Tag="588" IconVisible="False" LeftMargin="-715.0582" RightMargin="-420.9418" TopMargin="-301.9712" BottomMargin="-338.0288" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="1136.0000" Y="640.0000" />
                <AnchorPoint />
                <Position X="-715.0582" Y="-338.0288" />
                <Scale ScaleX="1.2538" ScaleY="1.0544" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="finalDialog" ActionTag="832981202" VisibleForFrame="False" Alpha="0" Tag="587" IconVisible="False" LeftMargin="-305.0000" RightMargin="-305.0000" TopMargin="-263.7487" BottomMargin="-131.2513" ctype="SpriteObjectData">
                <Size X="610.0000" Y="395.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="66.2487" />
                <Scale ScaleX="0.0001" ScaleY="0.0001" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="textures/FinalDlg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bouncingButton" ActionTag="1200085937" Tag="103" IconVisible="True" LeftMargin="-10.8556" RightMargin="10.8556" TopMargin="241.1318" BottomMargin="-241.1318" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="-10.8556" Y="-241.1318" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="BounceButton.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="logo" ActionTag="721920633" Tag="9" IconVisible="True" LeftMargin="-387.8959" RightMargin="387.8959" TopMargin="-255.6771" BottomMargin="255.6771" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="-387.8959" Y="255.6771" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Logo.csd" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="568.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="shadow" ActionTag="-1243310194" Tag="6" IconVisible="False" LeftMargin="-141.6647" RightMargin="141.6647" TopMargin="19.8846" BottomMargin="-19.8846" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1136.0000" Y="640.0000" />
            <AnchorPoint />
            <Position X="-141.6647" Y="-19.8846" />
            <Scale ScaleX="1.2538" ScaleY="1.0544" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.1247" Y="-0.0311" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>