<GameFile>
  <PropertyGroup Name="handPointer" Type="Node" ID="fd6c2632-fcf1-4927-8a71-e30b42b78245" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="80" Speed="1.0000" ActivedAnimationName="bouncing">
        <Timeline ActionTag="1610139305" Property="Position">
          <PointFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="35" X="30.0000" Y="15.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="50" X="30.0000" Y="15.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="80" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="bouncing" StartIndex="0" EndIndex="80">
          <RenderColor A="255" R="255" G="0" B="0" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Node" Tag="329" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="handPointer_1" ActionTag="1610139305" Tag="330" IconVisible="False" LeftMargin="-127.0000" RightMargin="-127.0000" TopMargin="-77.5000" BottomMargin="-77.5000" ctype="SpriteObjectData">
            <Size X="254.0000" Y="155.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="textures/handPointer.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>