<GameFile>
  <PropertyGroup Name="Logo" Type="Node" ID="766fa8df-58c1-434f-bbef-bcf669a27195" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="37" Speed="1.0000" ActivedAnimationName="intro">
        <Timeline ActionTag="-296323805" Property="Position">
          <PointFrame FrameIndex="0" X="-0.0050" Y="-0.0001">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="37" X="-0.0050" Y="-0.0001">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-296323805" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="15" X="1.3000" Y="1.3000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="24" X="1.0200" Y="1.0200">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="31" X="1.0800" Y="1.0800">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="37" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-296323805" Property="RotationSkew">
          <ScaleFrame FrameIndex="37" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-296323805" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="7" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="37" Value="255">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="intro" StartIndex="0" EndIndex="60">
          <RenderColor A="150" R="255" G="218" B="185" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Node" Tag="9" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="image" ActionTag="-296323805" Alpha="0" Tag="11" IconVisible="False" LeftMargin="-149.0050" RightMargin="-148.9950" TopMargin="-48.4999" BottomMargin="-48.5001" ctype="SpriteObjectData">
            <Size X="298.0000" Y="97.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0050" Y="-0.0001" />
            <Scale ScaleX="0.0010" ScaleY="0.0010" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="textures/Logo.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>